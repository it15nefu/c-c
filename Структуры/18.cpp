#include <iostream>
#include <string>

using namespace std;

struct Price {
    string name;
    string shopName;
    int price;
};

int mode;
Price *price = new Price[2];

void Mode ();
void Input();
void Find();

int main()
{
    Mode ();
    return 0;
}

void Mode ()
{
    cout << "Select mode\n0 - input; 1 - output\n";
    cin >> mode;
    if (mode == 0)
    {
        Input();
    }
    else if (mode == 1)
    {
        Find();
    }
}

void Input()
{
    cout << "\nInput order\nNAME SHOP_NAME PRICE\n";
    for (int i = 0; i < 2; i++)
    {
        cin >> price[i].name >> price[i].shopName >> price[i].price;
    }
    cout << "Input end\n\n";
    Mode();
}

void Find()
{
    string nameToFind;

    cout << "\nEnter name ";
    cin >> nameToFind;

    for (int i = 0; i < 2; i++)
    {
        if (price[i].name == nameToFind)
        {
            cout << price[i].name << " " << price[i].shopName << " " << price[i].price << "\n\n";
            Mode ();
            return;
        }
    }
    cout << nameToFind << " NOT FOUND\n\n";
    Mode();
}