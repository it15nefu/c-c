#include <fstream>
#include <iostream>
#include <cstdlib>
#include <windows.h>

using namespace std;

struct Pointer
{
	char word[255];
	int pages[10];
	int pages_num;
	Pointer *prev;
	Pointer *next;
};

Pointer * makeFirst()
{
	Pointer *ptr = new Pointer;
	cout << "Введите слово" << endl;
	cin >> ptr->word;
	cout << "Введите количество страниц" << endl;
	cin >> ptr->pages_num;
	while(ptr->pages_num>10 || ptr->pages_num<1)
	{
		cout << "Страниц больше 10 или меньше 1. Введите корректное количество страниц" << endl;
		cin >> ptr->pages_num;
	}
	for(int i=0; i<ptr->pages_num; i++)
	{
		cout << "Введите страницу" << endl;
		cin >> ptr->pages[i];
	}
	ptr->next = 0; ptr->prev = 0;
	return ptr;
}

void addToEnd(Pointer **pptr)
{
	Pointer *ptr = new Pointer;
	cout << "Введите слово" << endl;
	cin >> ptr->word;
	cout << "Введите количество слов" << endl;
	cin >> ptr->pages_num;
	for(int i=0; i<ptr->pages_num; i++)
	{
		cout << "Введите страницу" << endl;
		cin >> ptr->pages[i];
	}
	ptr->next = 0; ptr->prev = *pptr;
	(*pptr)->next = ptr;
	*pptr = ptr;
}

Pointer * find(Pointer * const bptr)
{
	char fword[255];
	cout << "Введите искомое слово" << endl;
	cin >> fword;

	Pointer *ptr = bptr;
	while(ptr)
	{
		if(!strcmp(ptr->word, fword))
			break;
		ptr = ptr->next;
	}
	return ptr;
}

Pointer * out(Pointer * const bptr)
{
	Pointer *ptr = bptr;
	while(ptr)
	{
		cout << "-------------------------------------------------------" << endl;
		cout << "Слово: " << ptr->word << endl;
		cout << "Количество слов" << ptr->pages_num << endl;
		cout << "Страницы: " << endl;
		for(int i=0; i<ptr->pages_num; i++)
			cout << ptr->pages[i] << " ";
		cout << endl << "-------------------------------------------------------" << endl;
		ptr = ptr->next;
	}
	return ptr;
}

int main()
{
	int m;

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	Pointer *start;
	Pointer *active;

	system("cls");

	while(1)
	{
		cout << "1. Сформировать список\n";
		cout << "2. Добавить указатель\n";
		cout << "3. Вывод всех указателей\n";
		cout << "4. Поиск\n";
		cout << "5. Выход\n";
		cin >> m;
		switch(m)
		{
			case 1: start = makeFirst(); active = start; break;
			case 2: addToEnd(&active); break;
			case 3: out(start); break;
			case 4: find(start); break;
			case 5: exit(0); break;
			default: cout << "Ошибка"; break;
		}
	}
}