#include <iostream>
#include <conio.h>
#include <stdlib.h>

using namespace std;

/* Динамические структуры данных. Вариант 18
 * Автоматизированная информационная система на ж/д вокзале содержит сведения об отправлении поездов дальнего следования.
 * Для каждого поезда указывается:
 * - номер поезда;
 * - станция назначения;
 * - время отправления;
 * Данные в информационной системе организованы в виде двоичного дерева.Написать программу, которая:
 * - обеспечивает первоначальный ввод данных в информационую систему и формирование двоичного дерева;
 * - производит вывод всего дерева;
 * - вводит номер поезда и выводит все данные об этом поезде;
 * - вводит название станции назначения и выводит данные о поездах, следующих до этой станции; */

// инициализация и определение структуры
struct train {
    // время отправления поезда
    unsigned short time;
    // номер поезда
    unsigned short number;
    // название станции
    string station;
    // указатели на левую и правую ветку дерева
    train *left, *right;
};

// функция добавления элемента в дерево
train* TrainAdd(train *beg, unsigned short number, unsigned short time, string station) {
    // если дерево пустое
    if (!beg) {
        train *temp = new train;
        temp->number = number;
        temp->time = time;
        temp->station = station;
        temp->left = 0;
        temp->right = 0;
        return temp;
    }
    // если дерево не пустое, начинается рекурсивный ввод в него
    // большие номера идут в левое поддерево
    else if (number > beg->number) {
        beg->right = TrainAdd(beg->right, number, time, station);
        return beg;
    }
    // меньшие номера идут в правое поддерево
    else {
        beg->left = TrainAdd(beg->left, number, time, station);
        return beg;
    }
    // в итоге элементы сортируются по возрастанию номера
}

// функция вывода всего дерева - рекурсивный обход слева на направо
void ShowAll(train *temp) {
    if (temp) {
        ShowAll(temp->left);
        cout << "Train number: " << temp->number << endl;
        cout << "Time: " << temp->time << endl;
        cout << "Station: " << temp->station << endl;
        ShowAll(temp->right);
    }
}

// поиск элемента с нужным номером, с учётом отсортированности дерева
void TrainFind(train *temp, unsigned short number, unsigned short &f) {
    if (temp) {
        if (temp->number < number)
            TrainFind(temp->right, number, f);
        else if (temp->number > number)
            TrainFind(temp->left, number, f);
        else {
            f = 1;
            cout << "Train number: " << temp->number << endl;
            cout << "Time: " << temp->time << endl;
            cout << "Station: " << temp->station << endl;
            return;
        }
    }
}

/* переменная f нужна для определения, был ли найден хотя бы 1 поезд т.к.
 * обход производится "втупую", т.к. дерево сортировано не по имени станции */
void StationShow(train *temp, string station, unsigned short &f) {
    if (temp) {
        if (station == temp->station) {
            f = 1;
            cout << "Train number: " << temp->number << endl;
            cout << "Time: " << temp->time << endl;
            cout << "Station: " << temp->station << endl;
        }
        StationShow(temp->right, station, f);
        StationShow(temp->left, station, f);
    }
}

int main() {
    // дерево, изначально являющееся пустым
    train *list = 0;

    char mode;

    unsigned short time;
    unsigned short number;
    string station;
    unsigned short f;

    while (1) {
        cout << "1 | Add train" << endl;
        cout << "2 | View all" << endl;
        cout << "3 | Find train by number" << endl;
        cout << "4 | Find train by station" << endl;
        cout << "5 | Clear screen" << endl;
        cout << "6 | Exit" << endl;

        cin >> mode;

        if (mode == '1') {
            cout << "Train number:" << endl;
            cin >> number;
            cout << "Time:" << endl;
            cin >> time;
            cout << "Station:" << endl;
            cin >> station;
            list = TrainAdd(list, number, time, station);
        }
        else if (mode == '2') {
            if (list)
                ShowAll(list);
            else
                cout << "There is no trains" << endl;
        }
        else if (mode == '3') {
            cout << "Enter a train number" << endl;
            cin >> number;
            f = 0;
            TrainFind(list, number, f);
            if (f == 0)
                cout << "There is no trains" << endl;
        }
        else if (mode == '4') {
            cout << "Enter a station name" << endl;
            cin >> station;
            f = 0;
            StationShow(list, station, f);
            if (f == 0)
                cout << "There is no trains" << endl;
        }
        else if (mode == '5') {
            system("cls");
        }
        else {
            exit(0);
        }
    }
}