#include <fstream>
#include <iostream>
#include <cstdlib>
#include <windows.h>

using namespace std;

struct TRAINS
{
	char destination[255];
	int number;
	int dep_time;
};

void vvod()
{
	fstream fin;
	fin.open("train.bin", ios::binary | ios::out | ios::app);
	struct TRAINS ptr;
	cout << "Введите путь для поезда" << endl;
	cin >> ptr.destination;
	cout << "Введите номер для поезда" << endl;
	cin >> ptr.number;
	cout << "Введите время отправления для поезда"<< endl;
	cin >> ptr.dep_time;
	fin.write((char*)&ptr, sizeof(TRAINS));
	fin.close();
}

void vivod(struct TRAINS *ptr)
{
	cout << "-----------------------------------------------\n";
	cout << "Номер поезда: " << ptr->number << endl;
	cout << "Пункт назначения: " << ptr->destination << endl;
	cout << "Время отбытия: " << ptr->dep_time << endl;
	cout << "-----------------------------------------------\n";
}

void vivod2()
{
	fstream fin;
	fin.open("train.bin", ios::binary | ios::in);
	struct TRAINS ptr;
	fin.read((char*)&ptr, sizeof(TRAINS));
	while(1)
	{
		if(fin.eof())
			break;
		vivod(&ptr);
		fin.read((char*)&ptr, sizeof(TRAINS));
	}
	fin.close();
}

void poisk()
{
	char a[255];
	cout << "Пункт назначения:\n";
	cin >> a;
	int f=0, k=0;
	struct TRAINS ptr, ptr1[1000], t;
	fstream fin;
	fin.open("train.bin", ios::binary | ios::in);
	while(!fin.eof())
	{
		fin.read((char*)&ptr, sizeof(TRAINS));
		if(!strcmp(ptr.destination, a))
		{
			f=1;
			ptr1[k] = ptr;
			k++;
		}
	}
	
	for(int i=0; i<k-1; i++)
		for(int j=0; j<k-i-1; j++)
			if(ptr1[j].dep_time>ptr1[j+1].dep_time)
			{
				t = ptr1[j];
				ptr1[j] = ptr1[j+1];
				ptr1[j+1] = t;
				cout << "Сортировка\n";
			}

	for(int i=0; i<k; i++)
		vivod(&ptr1[i]);

	if(!f)
		cout << "Поездов нет\n";
	fin.close();
}

int main()
{
	int m;

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	system("cls");

	while(1)
	{
		cout << "1. Ввести поезда\n";
		cout << "2. Поиск поездов\n";
		cout << "3. Вывод всех поездов\n";
		cout << "4. Выход\n";
		cin >> m;
		switch(m)
		{
			case 1: vvod(); break;
			case 2: poisk(); break;
			case 3: vivod2(); break;
			case 4: exit(0); break;
			default: cout << "Ошибка"; break;
		}
	}
}