#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void main()
{
	srand(time(NULL));

	FILE *input = NULL, *output = NULL;

	int t[20], n = 5, m = 10, buff, count;

	input = fopen("input.bin", "rb");
	output = fopen("output.bin", "wb");

	if (input == NULL || output == NULL)
	{
		printf("Error opening file");
	}

	//Random generation of "input.bin" file
	//for (int i = 0; i < n; i++)
	//{
	//	for (int j = 0; j < m; j++)
	//	{
	//		t[j] = rand() % 10 - 5;
	//		printf("%d ", t[j]);
	//	}
	//	printf("\n");
	//	fwrite(&t, sizeof(int), m, input);
	//}

	for (int i = 0; i < n; i++)
	{
		count = 0;
		for (int j = 0; j < m; j++)
		{
			fread(&buff, sizeof(int), 1, input);
			printf("%i ", buff);
			if (buff < 0)
			{
				count++;
			}
		}
		printf("%i\n ", count);
		fwrite(&count, sizeof(int), 1, output);
	}

	_fcloseall();
}