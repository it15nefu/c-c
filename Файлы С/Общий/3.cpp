#include <stdio.h>

int main()
{
	int n, min;
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
	scanf("%d", &n);
	int **a = new int*[n];
	for(int i=0; i<n; i++)
		a[i] = new int[n];
	int *b = new int[n];
	for(int i=0; i<n; i++)
		for(int j=0; j<n; j++)
			scanf("%d", &a[i][j]);
	for(int i=0; i<n; i++)
	{
		min = a[0][i];
		for(int j=0; j<n; j++)
			if(a[j][i] < min)
				min = a[j][i];
		printf("%d ", min);
	}
	printf("\n");
}