#include <stdio.h>
#include <string.h>

int main()
{
	float t[5][10], c[5][10]; int N[1], M[1], n, m, k[5] = {0,0,0,0,0};
	FILE *bin_out, *bin_in;
	
	bin_in = fopen("fin.bin", "rb");
	bin_out = fopen("fout.bin", "wb");
	
	fread(N, sizeof(int), 1, bin_in);
	fread(M, sizeof(int), 1, bin_in);
	n = N[0]; m = M[0];
	
	printf("%d, %d\n", n, m);
	
	for(int i=0; i<n; i++)
		fread(&t[i], sizeof(float), m, bin_in);
	
	for(int i=0; i<n; i++)
		for(int j=0; j<m; j++)
			if(t[i][j]<0)
			{
				c[i][k[i]] = t[i][j];
				k[i]++;
			}
	
	for(int i=0; i<n; i++)
		fwrite(&c[i], sizeof(float), k[i], bin_out);
	printf("\n");
}
