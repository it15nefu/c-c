#include <stdio.h>

int main()
{
	float s=0, t;
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
	while(!feof(stdin))
	{
		scanf("%f", &t);
		s += t;
	}
	printf("%f\n", s);
}