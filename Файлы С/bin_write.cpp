#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	srand(time(NULL));
	float t[20]; int n=5, m=10;
	FILE *bin_out;
	bin_out = fopen("fout.bin", "wb");
	fwrite(&n, sizeof(int), 1, bin_out);
	fwrite(&m, sizeof(int), 1, bin_out);
	for(int i=0; i<n; i++)
	{
		for(int j=0; j<m; j++)
		{
			t[j] = rand()%100 + (rand()%10/1000.0) - 50;
			printf("%f ", t[j]);
		}
		printf("\n");
		fwrite(&t, sizeof(float), m, bin_out);
	}
	
}