#include <stdio.h>
#include <string.h>

int main()
{
	int t[1000]; int a, b, max;
	FILE *bin_out, *bin_in;
	bin_in = fopen("fin.bin", "rb");
	fread(t, sizeof(int), 500, bin_in);
	scanf("%d %d", &a, &b);
	max = a;
	for(int i=a; i<b; i++)
		if(t[i] > t[max])
			max = i;
	printf("%d\n", t[max]);
}