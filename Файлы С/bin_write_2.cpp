#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	srand(time(NULL));
	int t[20]; int n=5, m=10;
	FILE *bin_out;
	bin_out = fopen("fout_i.bin", "wb");
	fwrite(&n, sizeof(int), 1, bin_out);
	fwrite(&m, sizeof(int), 1, bin_out);
	for(int i=0; i<n; i++)
	{
		for(int j=0; j<m; j++)
		{
			t[j] = rand()%100;
			printf("%d ", t[j]);
		}
		printf("\n");
		fwrite(&t, sizeof(int), m, bin_out);
	}
}