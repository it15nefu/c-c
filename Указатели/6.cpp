#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <windows.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C++
* Задание 6
*/

int* find(int **a, int n, int *b)
{
	int f=0;
	for(int i=0; i<n; i++)
	{
		f = 0;
		for(int j=0; j<n; j++)
			if(a[i][j]<0)
			{
				f = 1;
				break;
			}
		if(!f)
			for(int j=0; j<n; j++)
				b[i] += a[i][j];
		else
			b[i] = -1;
	}
	return b;
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n;
	scanf("%d", &n);
	int *s = new int[n];
	for(int i=0; i<n; i++)
		s[i] = 0;
	int **a = new int*[n];
	for(int i=0; i<n; i++)
		a[i] = new int[n];

	for(int i=0; i<n; i++)
		for(int j=0; j<n; j++)
			if(rand()%100>80)
				a[i][j] = rand()%10;
			else
				a[i][j] = rand()%10 - 2;

	for(int i=0; i<n; i++)
	{
		for(int j=0; j<n; j++)
			printf("%d ", a[i][j]);
		printf("\n");
	}
	
	find(a, n, s);

	for(int i=0; i<n; i++)
		if(s[i]>0)
			printf("i - %d | s - %d\n", i, s[i]);
}