#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <windows.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C++
* Задание 5
*/

int find(int **a, int n)
{
	int s=0;
	for(int i=0; i<n; i++)
	{
		s = 0;
		for(int j=0; j<n; j++)
			if(i!=j)
				s += a[i][j];
		printf("%d %d\n", a[i][i], s);
		if(a[i][i]<s)
			return NULL;
	}
	return 1;
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n;
	scanf("%d", &n);
	int **a = new int*[n];
	for(int i=0; i<n; i++)
		a[i] = new int[n];

	for(int i=0; i<n; i++)
	{
		for(int j=0; j<n; j++)
		{
			scanf("%d", &a[i][j]);
			//a[i][j] = rand()%10;
			//printf("%-2d ", a[i][j]);
		}
		printf("\n");
	}
	
	if(find(a, n))
		printf("Есть!\n");
	else
		printf("Нет\n");
}