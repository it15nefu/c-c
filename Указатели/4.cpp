#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <windows.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание 4
*/

int find(int *a, int n)
{
	for(int i=0; i<n; i++)
		for(int j=0; j<n; j++)
			if(abs(a[i])==abs(a[j]) && a[i]!=a[j])
				return 1;
	return NULL;
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n;
	scanf("%d", &n);
	int *a = (int*) malloc(n*sizeof(int));
	for(int i=0; i<n; i++)
	{
		//scanf("%d", &a[i]);
		a[i] = rand()%10-5;
		printf("%d ", a[i]);
	}
	printf("\n");

	if(find(a, n))
		printf("Есть!\n");
	else
		printf("Нет\n");
}