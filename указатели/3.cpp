#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <windows.h>
#include <time.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание 3
*/

int find(int *f, int *a, int n)
{
	int i=0;
	while(i<n)
	{
		if(*f==*a)
			return *a;
		a++; i++;
	}
	return NULL;
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
	int m, n;
	
	printf("N - ");
	scanf("%d", &n);
	int *a = (int*) malloc(n*sizeof(int));
	
	for(int i=0; i<n; i++)
	{
		a[i] = rand()%10;
		printf("%d ", a[i]);
	
	}
	printf("\nM - ");
	scanf("%d", &m);
	
	if(find(&m, a, n))
		printf("Есть!\n");
	else
		printf("Нет\n");
}