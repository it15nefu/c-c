#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание 2
*/

int main()
{
	int m, n, *p;
	scanf("%d", &n);
	p = &n;
	m = (*p) % 10;
	printf("%d\n", m);
}