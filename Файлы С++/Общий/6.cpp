#include <fstream>
#include <iostream>
#include <cstring>

using namespace std;

int main()
{
	ifstream fin;
	ofstream fout;
	fin.open("in.bin", ios::binary | ios::in);
	fout.open("out.bin", ios::binary | ios::out);

	int m=5, n=10, k=0; float s=0;
	float **a = new float*[m];
	for(int i=0; i<m; i++)
		a[i] = new float[n];
	float *b = new float[m*n];

	for(int i=0; i<m; i++)
		fin.read((char*)a[i], sizeof(float) * n);

	for(int i=0; i<m; i++)
	{
		s = 0;
		for(int j=0; j<n; j++)
			if(a[i][j]>0)
			{
				s += a[i][j];
				b[k] = a[i][j];
				k++;
			}
		s *= 2;
		cout << s << endl;
	}
			
	fout.write((char*)b, sizeof(float) * k);

	fin.close();
	fout.close();
}			