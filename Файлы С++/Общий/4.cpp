#include <fstream>
#include <iostream>
#include <cstring>

using namespace std;

int main()
{
	char a[100][1000], *pch;
	int n=0, k=0;

	ifstream fin;
	ofstream fout;
	fin.open("in.txt");
	fout.open("out.txt");
	while(!fin.eof())
	{
		fin.getline(a[n], 1000);
		n++;
	}

	for(int i=0; i<n; i++)
	{
		pch = strtok(a[i], " ");
		k = 0;
		while (pch!=NULL)
		{
			if(pch[0]==pch[strlen(pch)-1])
				k++;
			pch = strtok(NULL, " ");
		}
		fout << k << " ";
	}

	fin.close();
	fout.close();
}