#include <fstream>
#include <iostream>
#include <cstring>

using namespace std;

int main()
{
	char a[1000];
	int n=0;

	ifstream fin;
	ofstream fout;
	fin.open("in.txt");
	fout.open("out.txt");
	while(!fin.eof())
	{
		fin.getline(a, 1000);
		n = strlen(a);
		for(int i=0; i<n-1; i++)
			if(a[i]=='n' && a[i+1]=='o')
			{
				a[i] = 'o';
				a[i+1] = 'n';
			}
		fout << a;
	}

	fin.close();
	fout.close();
}			