#include <fstream>
#include <iostream>

using namespace std;

int main()
{
	int i, j, k, t, n=5;

	ifstream fin;
	ofstream fout;
	fin.open("in.txt");
	fout.open("out.txt");

	fin >> n;

	int *b = new int[n];
	int **a = new int*[n];
	for(i=0; i<n; i++)
		a[i] = new int[n];

	for(i=0; i<n; i++)
		for(j=0; j<n; j++)
			fin >> a[i][j];

	for(i=0; i<n; i++)
	{
		for(j=0; j<n; j++)
			cout << a[i][j] << " ";
		cout << endl;
	}
	printf("\n");

	for(k=0; k<n*n; k++)
		for(i=0; i<n; i++)
			for(j=0; j<n; j++)
				if(j<n-1)
				{
					if(a[i][j]>a[i][j+1])
					{
						t = a[i][j];
						a[i][j] = a[i][j+1];
						a[i][j+1] = t;
					}
				}
				else
				{
					if(i<n-1)
						if(a[i][j]<a[i+1][0])
						{
							t = a[i+1][0];
							a[i+1][0] = a[i][j];
							a[i][j] = t;
						}
				}

	for(i=0; i<n; i++)
	{
		for(j=0; j<n; j++)
			fout <<  a[i][j] << " ";
		fout << endl;
	}
}			