#include <fstream>
#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	float a, b, s1=0, s2=0;
	int t;
	ifstream fin1, fin2;
	fin1.open("in1.txt");
	fin2.open("in2.txt");
	while(!fin1.eof())
	{
		fin1.read((char*) &a, sizeof(float));
		t = (int) floorf(a);
		if(t%2)
			s1++;
	}
	
	while(!fin1.eof())
	{
		fin2.read((char*) &a, sizeof(float));
		t = (int) floorf(a);
		if(t%2)
			s2++;
	}

	fin1.close();
	fin2.close();

	if(s2>s1)
		cout << "File 1 have more numbers\n";
	else if(s1>s2)
		cout << "File 2 have more numbers\n";
	else if(s1==s2)
		cout << "File 1 and 2 have numbers\n";
	else 
		cout << "ERROR\n";
}