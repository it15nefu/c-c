#include <fstream>
#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	ifstream fin;
	ofstream fout;
	fin.open("in.bin", ios::binary | ios::in);
	fout.open("out.bin", ios::binary | ios::out);

	int m=5, n=10, k=0; float s=0;
	float **a = new float*[m];
	for(int i=0; i<m; i++)
		a[i] = new float[n];
	float *b = new float[m*n];

	for(int i=0; i<m; i++)
		fin.read((char*)a[i], sizeof(float) * n);

	for(int i=0; i<m; i++)
		for(int j=0; j<n; j++)
			a[i][j] = sin(a[i][j]);
			
	for(int i=0; i<m; i++)
		fout.write((char*)a[i], sizeof(float) * n);

	fin.close();
	fout.close();
}