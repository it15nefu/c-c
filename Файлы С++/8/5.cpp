#include <fstream>
#include <iostream>
#include <cstring>

using namespace std;

int main()
{
	ifstream fin;
	ofstream fout;
	fin.open("in.bin", ios::binary | ios::in);
	fout.open("out.bin", ios::binary | ios::out);

	int m=5, n=10, k=0; int s=0;
	int **a = new int*[m];
	for(int i=0; i<m; i++)
		a[i] = new int[n];
	int *b = new int[m*n];

	for(int i=0; i<m; i++)
		fin.read((char*)a[i], sizeof(int) * n);

	for(int i=0; i<m; i++)
	{
		s = 0;
		for(int j=0; j<n; j++)
			s += a[i][j];
		fout.write((char*)&s, sizeof(int));
	}

	fin.close();
	fout.close();
}