#include <fstream>
#include <iostream>

using namespace std;

int main()
{
	char a[100][1000];
	int max, imax, n=0, t;
	ifstream fin;
	fin.open("in.txt");

	while(!fin.eof())
	{
		fin.getline(a[n], 1000);
		n++;
	}
	
	max = strlen(a[0]); imax = 0;
	for(int i=0; i<n; i++)
	{
		t = strlen(a[i]);
		if(max<t)
		{
			max = t;
			imax = i;
		}
	}

	cout << a[imax] << endl;
}