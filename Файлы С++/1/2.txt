#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int main()
{
	ifstream infile("input.txt");

	string temp;
	float tempFloat;
	float max = NULL;
	int count = -1;

	if (!infile.is_open())
	{
		cerr << "error" << endl;
		return 1;
	}

	while (!infile.eof())
	{
		infile >> temp;
		tempFloat = (float)atof(temp.c_str());
		
		if (max == NULL || max < tempFloat)
			max = tempFloat;	
	}

	infile.seekg(0);

	while (!infile.eof())
	{
		infile >> temp;
		tempFloat = (float)atof(temp.c_str());

		if (tempFloat == max)
			count++;
	}

	cout << count << endl;

	infile.close();

	return 0;
}