#define _USE_MATH_DEFINES
//�������� �1-1
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <math.h>
#include <time.h>
#include <iostream>

int main()
{
	setlocale(LC_ALL, "Russian");
	srand(time(NULL));
	int i, j, s=0, n=5;

	int **a = new int*[n];
	for(i=0; i<n; i++)
		a[i] = new int[n];

	for(i=0; i<n; i++)
		for(j=0; j<n; j++)
			a[i][j] = rand()%10;

	for(i=0; i<n; i++)
	{
		for(j=0; j<n; j++)
			printf("%d ", a[i][j]);
		printf("\n");
	}

	for(i=0; i<n; i++)
		for(j=0; j<n; j++)
			s += a[i][j];

	printf("%d\n", s);
}