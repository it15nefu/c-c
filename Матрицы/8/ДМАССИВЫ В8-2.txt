#define _USE_MATH_DEFINES
//�������� �9-2
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <math.h>
#include <time.h>
#include <iostream>

int main()
{
	setlocale(LC_ALL, "Russian");
	srand(time(NULL));
	int i, j, min, n=5;

	int **a = new int*[n];
	for(i=0; i<n; i++)
		a[i] = new int[n];

	for(i=0; i<n; i++)
		for(j=0; j<n; j++)
			a[i][j] = 10 - rand()%20;
	
	for(i=0; i<n; i++)
	{
		for(j=0; j<n; j++)
			printf("%2d ", a[i][j]);
		printf("\n");
	}
	printf("=========================\n");
	for(i=0; i<n; i++)
	{
		for(j=0; j<n; j++)
		{
			if(a[i][j] < 0)
				a[i][j] = -1;
			printf("%2d ", a[i][j]);
		}
		printf("\n");
	}
}