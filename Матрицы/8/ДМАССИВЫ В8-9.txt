#define _USE_MATH_DEFINES
//�������� �8-9
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <math.h>
#include <time.h>
#include <iostream>

int main()
{
	setlocale(LC_ALL, "Russian");
	srand(time(NULL));
	int i, j, n, x, y;

	scanf("%d %d %d", &n, &x, &y);

	float **a = new float*[n];
	for(i=0; i<n; i++)
		a[i] = new float[n];
	float *b = new float[n];
	float *c = new float[n];

	for(i=0; i<n; i++)
		for(j=0; j<n; j++)
			a[i][j] = rand()%100 + (rand()%100)*0.01;

	for(i=0; i<n; i++)
	{
		for(j=0; j<n; j++)
			printf("%-6.2f ", a[i][j]);
		printf("\n\n");
	}

	printf("===================================\n");

	for(i=0; i<n; i++)
		b[i] = a[x][i];

	for(i=0; i<n; i++)
		c[i] = a[i][y];

	for(i=0; i<n; i++)
		a[x][i] = c[i];

	for(i=0; i<n; i++)
		a[i][y] = b[i];

	for(i=0; i<n; i++)
	{
		for(j=0; j<n; j++)
			printf("%-6.2f ", a[i][j]);
		printf("\n\n");
	}
}