#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int k = 0, l = 0;
    char s[256], s1[256];
    printf("S = ");

    gets(s);

    l = strlen(s);

    for (int i = 0; i < l; i++)
    {
        if (s[i] == '.')
        {
            s1[k] = '.';
            s1[k + 1] = ' ';
            ++k;
        }
        else
            s1[k] = s[i];

        k++;
    }

    printf("S = %s", s1);

    return 0;
}