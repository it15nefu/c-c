#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int k = 0;
    char s[256];
    printf("S = ");

    gets(s);

    for (int i = 0; s[i] != '\0'; i++)
        if (s[i] == 'a')
            k++;
        else if (s[i] == 'b')
            k--;

    if (k > 0)
        printf("the 'a' character is more common ");
    else if (k < 0)
        printf("the 'b' character is more common ");
    else
        printf("both of the characters occurs equally often ");


    return 0;
}