#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int l = 0, count = 0;
    char s[256];
    printf("S = ");

    gets(s);

    l = strlen(s);

    for (int j = 0; j < l; j++)
    {
        for (int i = 0; i < l - 1; i++)
            if (s[i] == ' ' && s[i + 1] == ' ')
            {
                count++;
                for (int j = i; j < l; j++)
                {
                    s[j] = s[j + 1];
                }
                i--;
            }
    }

    s[l - count] = '\0';

    printf("S = %s", s);

    return 0;
}
