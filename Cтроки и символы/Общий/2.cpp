﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание 2
*/

int main()
{
	setlocale(LC_ALL, "Russian");
	char s[256];
	int i, j, l, L, R, k=0;

	printf("S: \n");
	gets(s);
	l = strlen(s);
	printf("Длина строки = %d\n", l);

	while(!k)
	{
		for(i=0; i<l; i++)
		{
			if(s[i]=='(')
				L = i;
			if(s[i]==')' && L)
			{
				R = i;
				break;
			}
		}

		if(!L && !R)
		{
			k = 1;
			break;
		}
		
		if(L && R)
			printf("L = %d; R = %d;\n", L, R);

		for(i=L; i<=R; i++)
		{
			for(j=L; j<l-1; j++)
				s[j] = s[j+1];
			l--;
			s[l] = '\0';
		}
		printf("%s\n", s);

		R = 0; L = 0;
	}
}