#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int l, fC = 0, lC = 0;
    char s[256], tmp;
    printf("S = ");

    gets(s);
    l = strlen(s);

    for (int i = 0; i < l; i++)
    {
        if (s[i] != ' ' && (s[i + 1] == ' ' || s[i + 1] == '\0'))
        {
            lC = i;

            for (int j = fC, k = lC; j < k; j++, k--)
            {
                tmp = s[j];
                s[j] = s[k];
                s[k] = tmp;
            }
            fC = i + 2;
        }
    }

    printf("S = %s", s);

    return 0;
}