#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <windows.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание 4
*/

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	char s[256];
	int i, l, L, R, t, j, k;
	printf("S: \n");
	gets(s);
	l = strlen(s);
	
	if(s[0]!=' ')
		L = 0;
	for(i=0; i<l; i++)
	{	
		if(s[i]==' ' && (s[i+1]!=' ' || s[i+1]!='\0'))
			L = i+1;
		if(s[i]!=' ' && (s[i+1]==' ' || s[i+1]=='\0'))
			R = i+1;
		for(j=L, k=R-1; j<k; j++, k--)
		{
			t = s[j];
			s[j] = s[k];
			s[k] = t;
		}
	}
	printf("%s\n", s);
}