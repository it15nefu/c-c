﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание 8
*/

int main()
{
	setlocale(LC_ALL, "Russian");		
	char s[256], a[256], b[256];				
	int i, l, k=0, q=0;				
	printf("S: \n");				
	gets(s);				
	l = strlen(s);				

	//8.a
	for(i=0; i<l; i++)			
		if(s[i]!='*')
			a[i] = s[i];
		else
			break;

	printf("%s\n", a);

	//8.b
	for(i=0; i<l; i++)
		if(s[i]=='*')
			k = i;

	for(i=k+1; i<l; i++)
	{
		b[q] = s[i];
		q++;
	}

	printf("%s\n", b);
}