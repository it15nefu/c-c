﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание 3
*/

int main()
{
	setlocale(LC_ALL, "Russian");
	char s[256];
	int i, j, l, L, R, k=0;

	printf("S: \n");
	gets(s);
	l = strlen(s);
	printf("Длина строки = %d\n", l);

	for(i=0; i<l-1; i++)
		if(s[i]==' ' && s[i+1]!=' ')
			k++;
	if(s[0]!=' ')
		k++;

	printf("Количество слов = %d\n", k);
}