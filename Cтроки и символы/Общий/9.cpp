﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание 9
*/

int main()
{
	setlocale(LC_ALL, "Russian");
	char s[256], a[256], b[256];
	int i, l, k=0, max=0;;
	printf("S: \n");
	gets(s);
	l = strlen(s);

	for(i=0; i<l-1; i++)
		if(s[i]=='+' && s[i+1]=='+')
		{
			if(!k)
				k=2;
			else
				k++;
			if(k>max)
				max++;
		}
		else
			k=0;
		
	printf("%d\n", max);
}