#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    char s[256] = "This (is) a (brackets) destroyer (example)";
    int l, left, right, state = 0;

    printf("%s\n", s);

    //gets(s);
    l = strlen(s);

    while (!state)
    {
        for (int i = 0; i < l; i++)
        {
            if (s[i] == '(')
                left = i;
            if (s[i] == ')' && left)
            {
                right = i;
                break;
            }
        }

        if (!left && !right)
        {
            state = 1;
            break;
        }

        for (int i = left; i <= right; i++)
        {
            for(int j = left; j < l - 1; j++)
                s[j] = s[j + 1];
            l--;
            s[l] = '\0';
        }

        right = 0; left = 0;
    }

    printf("%s\n", s);
}
