#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <windows.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание 5
*/

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);	
	char s[256];				
	int i, l, a=0, b=0;	
	printf("S: \n");				
	gets(s);
	l = strlen(s);				
	
	for(i=0; i<=l; i++)
	{
		if(s[i]=='a')
			a++;
		if(s[i]=='b')
			b++;
	}

	if(a>b)
		printf("A встречается чаще чем B\n");
	else if(a<b)
		printf("B встречается чаще чем A\n");
	else if(a==b)
		printf("A и B встречаются одинаково");
	else
		printf("КАКАЯ_ТО_ОШИБКА!");
}