#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int l = 0, p = 0;
    char s[256];
    printf("S = ");

    gets(s);
    l = strlen(s);

    for (int i = l; i > 0 ; i--)
    {
        if (s[i] == '*')
        {
            p = i + 1;
            break;
        }
    }

    while (p < l)
    {
        printf("%c", s[p]);
        p++;
    }

    return 0;
}
