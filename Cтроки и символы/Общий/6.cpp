#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание 6
*/

int main()
{
	setlocale(LC_ALL, "Russian");
	char s[256];
	int i, l, L, k=0, j=0;
	printf("S: \n");
	gets(s);
	l = strlen(s);
	
	for(i=1; i<l;)
		if(s[i-1]=='c' && s[i]=='b')
		{
			for(j=i; j<l; j++)
				s[j] = s[j+1];
			s[l] = '\0';
			l--;
		}
		else
			i++;
	
	printf("%s\n", s);
}