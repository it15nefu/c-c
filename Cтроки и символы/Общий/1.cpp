﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание 1
*/

int main()
{
	setlocale(LC_ALL, "Russian");
	char s[256];
	int i, j, l, L, k=0;

	printf("S: \n");
	gets(s);
	l = strlen(s);
	printf("Длина строки = %d\n", l);

	//1.a
	for(i=0; i<l; i++)
		if(s[i]==' ')
			k++;
	
	printf("Количество пробелов = %d\n", k);


	//1.b
	k = 0;
	for(i=0; i<l-2; i++)
		if(s[i+0]=='p' &&
		   s[i+1]=='a' &&
		   s[i+2]=='r')
		{
			k = 1;
			break;
		}
	if(k)
		printf("Есть par.\n");
	else
		printf("Нет par!\n");


	//1.c
	k = 0;
	for(i=0; i<l-1; i++)
		if(s[i+0]==s[i+1])
		{
			k = 1;
			break;
		}
	if(k)
		printf("Есть пара.\n");
	else
		printf("Нет пар!\n");


	//1.d
	L = l; char b[256];

	for(i=0; i<l; i++)
		b[i] = s[i];

	b[l] = '\0';
	for(i=0; i<L; i++)
		if(b[i]=='a')
		{
			L--;
			for(j=i; j<L; j++)
				b[j] = b[j+1];
			b[L] = '\0';
		}
	printf("%s\n", b);


	//1.e Заменить все . на !
	L = l; char c[256];

	for(i=0; i<l; i++)
		c[i] = s[i];
	c[l] = '\0';

	for(i=0; i<l; i++)
		if(c[i]=='.')
			c[i]='!';

	printf("%s\n", c);


	//1.f Заменить все '  ' на ' '
	L = l; char d[256];

	for(i=0; i<l; i++)
		d[i] = s[i];
	d[l] = '\0';

	for(i=0; i<L;)
		if(d[i+0]==' ' && d[i+1]==' ')
		{
			for(j=i; j<L-1; j++)
				d[j] = d[j+1];
			L--;
			d[L] = '\0';
		}
		else
			i++;

	printf("%s\n", d);
}