#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <windows.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание 12
*/

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	char s[256];
	int i, l, L, k=0, j=0, r=0;
	printf("S: \n");
	gets(s);
	l = strlen(s);
	
	//1
	for(i=0; i<l-1; i++)
		if(s[i]==' ' && s[i+1]!=' ')
			k++;
	if(s[0]!=' ')
		k++;

	printf("1: Слов в строке - %d\n", k);

	//2
	k = 0;
	for(i=0; i<l-1; i++)
		if(s[i]==' ' && s[i-1]=='а')
			k++;
	if(s[0]=='а')
		k++;

	printf("2: Слов, начинающихся с 'a' в строке - %d\n", k);

	//3
	k = 0;
	for(i=1; i<l; i++)
		if(s[i]=='ь' && s[i+1]==' ')
			k++;
	if(s[l]=='ь')
		k++;

	printf("3: Слов, заканчивающихся на 'ь' в строке - %d\n", k);

	//4/5/6
	int c, f, t;
	int t_min, t_max, c_min, c_max, e_min, e_max;
	int c_end, e_end, z;
	char b[256]; 
	c = t = j = k = f = 0;
	t_min = t_max = e_min = e_max = c_min = c_max = 0;
	c_end = e_end = z = 0;
	for(i=0; i<=l; i++)
	{
		if(s[i]!=' ')
			f = 1;
		if(s[i]==' ' || s[i]=='\0')
		{
			if(f)
			{
				b[k] = '\0';
				k = 0;
			}
			f = 0;
		}
		if(f)
		{
			b[k] = s[i];
			k++;
		}
		if(!f)
		{
			if(!t_max)
			{
				t_max = strlen(b);
				c_max = i-t_max;
				e_max = i;
			}
			if(!t_min)
			{
				t_min = strlen(b);
				c_min = i-t_min;
				e_min = i;
			}
			t = strlen(b);
			if(t>t_max)
			{
				t_max = t;
				c_max = i-t;
				e_max = i;
			}
			if(t<t_min)
			{
				t_min = t;
				c_min = i-t;
				e_min = i;
			}
			if(b[0]==b[t-1] && !z)
			{
				c_end = i-t;
				e_end = i;
				z = 1;
			}
		}
	}

	printf("4: Самое длинное слово - ");
	for(i=c_max; i<e_max; i++)
		putchar(s[i]);
	printf("\n");
	
	printf("5: Самое короткое слово - ");
	for(i=c_min; i<e_min; i++)
		putchar(s[i]);
	printf("\n");

	if(z)
	{
		printf("6: Слово с одинаковыми символами в начале и конце - ");
		for(i=c_end; i<e_end; i++)
			putchar(s[i]);
		printf("\n");
	}
	else
		printf("6: Нет такого слова\n");

	int C[256];
	for(i=0, j=l-1; i<l, j>=0; i++, j--)
		C[j] = s[i];
	c[j] = '\0';

	printf("7: ");
	c = j = k = t = f = 0;
	for(i=0; i<=l; i++)
	{
		if(C[i]!=' ' || C[i]!='\0')
			f = 1;
		if(C[i]==' ' || C[i]=='\0')
		{
			if(f)
			{
				j++;
				b[k] = '\0';
				k = 0;
			}
			f = 0;
		}
		if(f)
		{
			b[k] = C[i];
			k++;
		}
		if(!f)
		{
			L = strlen(b);
			for(t=L-1; t>=0; t--)
				putchar(b[t]);
			printf(" ");
		}
	}

	printf("\n");
}