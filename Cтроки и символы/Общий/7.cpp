#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание 7
*/

int main()
{
	setlocale(LC_ALL, "Russian");
	char s[256];
	int i, l, L, k=0, e=0, y=0, j=0;
	printf("S: \n");
	gets(s);
	l = strlen(s);
	
	for(i=0; i<l; i++)
		if(s[i]=='k')
			k=1;
		else if(s[i]=='e')
			e=1;
		else if(s[i]=='y')
			y=1;
	
	if(k && e && y)
		printf("Есть\n");
	else
		printf("Нет");
}