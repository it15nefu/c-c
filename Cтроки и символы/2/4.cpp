﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание В-2, 4
*/

int main()
{
	setlocale(LC_ALL, "Russian");
	char s[256];
	int i, j, l, L, k=0;
	printf("S: \n");
	gets(s);
	l = strlen(s);

	for(i=0; i<l; i++)
		if(s[i]=='!')
		{
			l += 2;
			for(j=l-1; j>i+2; j--)
				s[j] = s[j-2];
			s[i]='.';
			s[i+1]='.';
			s[i+2]='.';
			s[l] = '\0';
		}

	printf("%s\n", s);
}