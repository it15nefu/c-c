#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание В-2, 2
*/

int main()
{
	setlocale(LC_ALL, "Russian");
	char a[256], b[256], c[256];
	int i, la, lb;
	printf("A: \n");
	gets(a);
	la = strlen(a);
	printf("B: \n");
	gets(b);
	lb = strlen(b);
	if(la!=lb)
	{
		printf("ОШИБКА!\n");
		return 1;
	}
	
	for(i=0; i<la; i++)
	{
		if(a[i]>b[i])
			c[i] = a[i];
		else
			c[i] = b[i];
	}
	c[i] = '\0';
	
	printf("%s \n", c);
}