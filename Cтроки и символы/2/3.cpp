#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <windows.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание В-2, 3
*/

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	char s[256];
	int i, l, k, j;
	printf("S: \n");
	gets(s);
	l = strlen(s);
	
	int c, f, t, t_min, c_min, e_min;
	char b[256]; 
	c = t = j = k = f = 0;
	t_min = e_min = c_min = 0;
	for(i=0; i<=l; i++)
	{
		if(s[i]!=' ')
			f = 1;
		if(s[i]==' ' || s[i]=='\0')
		{
			if(f)
			{
				b[k] = '\0';
				k = 0;
			}
			f = 0;
		}
		if(f)
		{
			b[k] = s[i];
			k++;
		}
		if(!f)
		{
			if(!t_min)
			{
				t_min = strlen(b);
				c_min = i-t_min;
				e_min = i;
			}
			t = strlen(b);
			if(t<t_min)
			{
				t_min = t;
				c_min = i-t;
				e_min = i;
			}
		}
	}
	printf("Самое короткое слово - ");
	for(i=c_min; i<e_min; i++)
		putchar(s[i]);
	printf("\n");
}