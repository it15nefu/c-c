#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

/*
* Компилятор: MS VC++ 2008 eX
* Синтаксис: MS C
* Задание B-2, 1
*/

int main()
{
	setlocale(LC_ALL, "Russian");
	char s[256];
	int i, l, L, k=0, r=0, t=0;
	printf("S: \n");
	gets(s);
	l = strlen(s);
	
	for(i=0; i<l; i++)
		if(s[i]=='r')
			r++;
		else if(s[i]=='k')
			k++;
		else if(s[i]=='t')
			t++;
	
	
	printf("r - %d | k - %d | t - %d\n", r, k, t);
}