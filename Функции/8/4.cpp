#include <iostream>
#include <iomanip>
#include <locale>
#include <windows.h>

using namespace std;

/*
* Компилятор: MS VC++ 2008 eX
* Совместимость: Все допустимые компиляторы C++
* Задание 5
*/

void array_fill(int **a, int n, int m)
{
	for(int i=0; i<n; i++)
	{
		for(int j=0; j<m; j++)
		{
			a[i][j] = rand()%10;
			cout << a[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

void array_out(int **a, int n, int m)
{
	for(int i=0; i<n; i++)
	{
		for(int j=0; j<m; j++)
		{
			cout << a[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

void array_copy(int **a, int **b, int n, int m)
{
	for(int i=0; i<n; i++)
		for(int j=0; j<m; j++)
			b[i][j] = a[i][j];
}

void matrix_T(int **aR, int n, int m)
{
	int t;
	for(int i=0; i<n; i++)
		for(int j=i; j<m; j++)
		{
			t = aR[i][j];
			aR[i][j] = aR[j][i];
			aR[j][i] = t;
		}
}

int array_check(int **a, int **b, int n, int m)
{
	for(int i=0; i<n; i++)
		for(int j=0; j<m; j++)
			if(a[i][j] != b[i][j])
				return 0;
	return 1;
}

int main()
{
	int n; //n строк
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите размер матрицы" << endl;
	cin >> n;
	int **a = new int*[n];
	int **b = new int*[n];
	for(int i=0; i<n; i++)
	{
		a[i] = new int[n];
		b[i] = new int[n];
	}
	cout << "A:" << endl;
	array_fill(a, n, n);
	array_copy(a, b, n, n);

	cout << "AT:" << endl;
	matrix_T(b, n, n);
	array_out(b, n, n);

	cout << "ATT:" << endl;
	matrix_T(b, n, n);
	array_out(b, n, n);

	if(array_check(a, b, n, n))
		cout << "Матрицы A и (AT)T совпадают" << endl;
	else
		cout << "Матрицы A и (AT)T несовпадают" << endl;
}