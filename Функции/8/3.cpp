#include <iostream>
#include <locale>
#include <windows.h>

using namespace std;

/*
* Компилятор: MS VC++ 2008 eX
* Совместимость: Все допустимые компиляторы C++
* Задание В-8, 3
* Заметка: 
* Используется решето Эратосфена в качестве функции для определения простых чисел. 
* Жрет память, зато ОЧЕНЬ быстро.
*/

int set_prime_numbers(char *a, char *b, int n)
{
	int k = 0;
	for(int i=0; i<n; i++)
	{
		a[i] = 0;
		b[i] = 0;
	}
	for(int i=2; i<n; i++)
	{
		if(a[i]==0)
		{
			a[i] = 1;
			b[k] = i;
			k++;
			for(int j=2; (i*j)<n; j++)
				a[i*j] = -1;
		}
	}
	return k;
}

void out_prime_pairs(char *b, int n)
{
	for(int i=1; i<n; i++)
		if(b[i]-b[i-1]==2)
			cout << (int) b[i-1] << " " << (int) b[i] << endl;
}

int main()
{
	int n, k;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите n" << endl;
	cin >> n;
	n *= 2;
	n += 1;
	char *a = new char[n];
	char *b = new char[n];
	k = set_prime_numbers(a, b, n);
	out_prime_pairs(b, k);
}