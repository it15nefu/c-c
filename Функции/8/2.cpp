#include <iostream>
#include <locale>
#include <windows.h>

using namespace std;

/*
* Компилятор: MS VC++ 2008 eX
* Совместимость: Все допустимые компиляторы C++
* Задание В-8, 2
*/

double sign(double x)
{
	if(x>0)
		return 1;
	if(x==0)
		return 0;
	if(x<0)
		return -1;
}

int main()
{
	double x, y;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите x и y" << endl;
	cin >> x >> y;
	cout << (sign(x)+sign(y))*sign(x+y) << endl;
}