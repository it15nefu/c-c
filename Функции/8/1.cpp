#include <iostream>
#include <iomanip>
#include <locale>
#include <windows.h>

using namespace std;

/*
* Компилятор: MS VC++ 2008 eX
* Совместимость: Все допустимые компиляторы C++
* Задание В-8, 1
*/

double dohod(double f_money, double percents, double years)
{
	double e_money = 0;
	percents /= 100;
	e_money = f_money * percents * years;
	return e_money;
}

int main()
{
	double e_money, f_money, percents, years;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите размер вклада, процент годовых и срок вклада в годах" << endl;
	cin >> f_money >> percents >> years;
	e_money = dohod(f_money, percents, years);
	cout << "Доход по вкладу = " << e_money << endl;
}