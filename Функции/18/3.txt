#include <iostream>
#include <stdlib.h>

using namespace std;

float Add(float _a, float _b, float _c, float _d);

int main()
{
    float a, b, c, d;

    cin >> a >> c;
    cout << "-+-\n";
    cin >> b >> d;
    cout << "\n" << Add(a, b, c, d);

    return 0;
}

float Add(float _a, float _b, float _c, float _d)
{
    return (_a * _d + _b * _c) / (_b * _d);
}