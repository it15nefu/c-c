#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int** MatrixCreate (const int size);
int** MatrixRandomize (int** mtrx, const int size);
int** MatrixMultiply (int** mtrxA, int** mtrxB, const int size);
void MatrixPrint(int** arr, const int size);

int main()
{
    srand(time(NULL));

    int n;

    printf("Size: ");
    scanf("%i", &n);

    int** mtrxA = MatrixCreate(n);
    mtrxA = MatrixRandomize(mtrxA, n);
    MatrixPrint(mtrxA, n);

    int** mtrxB = MatrixCreate(n);
    mtrxB = MatrixRandomize(mtrxB, n);
    MatrixPrint(mtrxB, n);

    int** mtrxC = MatrixMultiply(mtrxA, mtrxB, n);
    MatrixPrint(mtrxC, n);

    return 0;
}

int** MatrixCreate (const int size)
{
    int** mtrx = new int*[size];

    for(int i = 0; i < size; ++i)
       mtrx[i] = new int[size];

    return mtrx;
}

int** MatrixRandomize (int** mtrx, const int size)
{
    for(int i = 0; i < size; i++)
       for(int j = 0; j < size; j++)
           mtrx[i][j] = rand() % 10;

    return mtrx;
}

int** MatrixMultiply (int** mtrxA, int** mtrxB, const int size)
{
    int sum;

    int** mtrx = MatrixCreate(size);

    for (int i = 0; i < size; i++)
        for (int j = 0; j < size; j++)
        {
            sum = 0;
            for (int k = 0; k < size; ++k)
                sum += mtrxA[i][k] * mtrxB[k][j];
            mtrx[i][j] = sum;
        }

    return mtrx;
}

void MatrixPrint(int** arr, const int size)
{
   for(int i = 0; i < size; i++)
   {
       for(int j = 0; j < size; j++)
           printf("%i ", arr[i][j]);
       printf("\n");
   }
   printf("\n");
}