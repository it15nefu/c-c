#include <iostream>
#include <locale.h>
#include <windows.h>

using namespace std;

/*
* Компилятор: MS VC++ 2008 eX
* Совместимость: Все допустимые компиляторы C++
* Задание 4
*/

void array_fill(int **a, int n, int m)
{
	for(int i=0; i<n; i++)
	{
		for(int j=0; j<m; j++)
		{
			a[i][j] = rand()%10;
			cout << a[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

int array_sum(int **a, int n, int m)
{
	int s=0;
	for(int i=0; i<n; i++)
		for(int j=0; j<m; j++)
			s += a[i][j];
	return s;
}

int main()
{
	int n, m; //n строк, m столбцов
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите размер матрицы (строки, столбцы)" << endl;
	cin >> n >> m;
	int **a = new int*[n];
	for(int i=0; i<n; i++)
		a[i] = new int[m];
	array_fill(a, n, m);
	cout << "Сумма элементов - " << array_sum(a, n, m) << endl;
}