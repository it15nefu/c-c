#define _USE_MATH_DEFINES 1

#include <iostream>
#include <locale.h>
#include <windows.h>
#include <math.h>

using namespace std;

/*
* Компилятор: MS VC++ 2008 eX
* Совместимость: Все допустимые компиляторы
* Задание 2
*/

double f(double x, double y)
{
	double V = sinh(x) * tan(2*x+1) - pow(tan(1 + sinh(x+1)), 2);
	return V;
}

int main()
{
	double x, y;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите X и Y" << endl;
	cin >> x >> y;
	cout << "F = " << f(x, y) << endl;
}