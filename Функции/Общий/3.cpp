#define _USE_MATH_DEFINES 1

#include <iostream>
#include <locale.h>
#include <windows.h>
#include <math.h>

using namespace std;

/*
* Компилятор: MS VC++ 2008 eX
* Совместимость: Все допустимые компиляторы
* Задание 3
*/

void array_fill(int *a, int n)
{
	for(int i=0; i<n; i++)
	{
		a[i] = rand()%10;
		cout << a[i] << " ";
	}
	cout << endl;
}

int array_min(int *a, int n)
{
	int min = a[0], i_min = 0;
	for(int i=0; i<n; i++)
		if(a[i] < min)
		{
			i_min = i;
			min = a[i];
		}
	return i_min;
}

int array_max(int *a, int n)
{
	int max = a[0], i_max = 0;
	for(int i=0; i<n; i++)
		if(a[i] > max)
		{
			i_max = i;
			max = a[i];
		}
	return i_max;
}

int main()
{
	int n, min, max;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите размер массивов" << endl;
	cin >> n;
	int *a = new int[n];
	array_fill(a, n);
	min = array_min(a, n);
	max = array_max(a, n);
	cout << "Минимум - " << a[min] << " расположен на - " << min << endl;
	cout << "Максимум - " << a[max] << " расположен на - " << max << endl;
	cout << a[min]+a[max] << endl;
}