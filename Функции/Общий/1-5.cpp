/*
* Совместимость: Все допустимые компиляторы
* Писал Альберт
*/

//////////////////////////////////////////////////////////1
#include <iostream>
#include <locale>
using namespace std;

void EnterHR(double &h, double &r)
{
	cout << "Введите h и r = ";
	cin >> h >> r;
}

void SearchV(double h, double r, double &v)
{
	v=(h * r*r * 3.14);
}

void AnswerV(double v)
{
	cout << "V = " << v << endl;
}

void main()
{
	setlocale(LC_ALL, "RU");
	cout << "7|1 Объем" << endl << endl;
	double h, r, v;
	EnterHR(h, r);
	SearchV(h, r, v);
	AnswerV(v);
}
//////////////////////////////////////////////////////////2
#include <iostream>
#include <math.h>
#include <locale>
using namespace std;

void EnterX(double &x)
{
	cout << "Введите x = ";
	cin >> x;
}

void SearchZ(double x, double &z)
{
	z=(sinh(x)*tan(2*x+1)-tan(1 + sinh(x + 1))*tan(1+sinh(x+1)));
}

void AnswerZ(double z)
{
	cout << "Ответ z = " << z << endl;
}

void main()
{
	setlocale(LC_ALL, "RU");
	cout << "7|2 Функция" << endl << endl;
	double x,z;
	EnterX(x);
	SearchZ(x,z);
	AnswerZ(z);
}
//////////////////////////////////////////////////////////3
#include <iostream>
#include <locale>
using namespace std;

void EnterN(int &n)
{
	cout << "Введите N = ";
	cin >> n;
	cout << endl;
}

void EnterAr(int ar[], int k, int n)
{
	cout << "Введите Массив №"<< k << ": ";
	for (int i = 0; i < n; i++)
		cin >> ar[i];
}

void SearchMin(int ar[], int &min, int n)
{
	min = ar[0];
	for (int i = 0; i < n; i++)
		if (ar[i] < min)
			min = ar[i];
}

void SearchMax(int ar[], int &max, int n)
{
	max = ar[0];
	for (int i = 0; i < n; i++)
		if (ar[i] > max)
			max = ar[i];
}

void SearchZ(int min, int max, int &z)
{
	z = min + max;
}

void Answer(int z)
{
	cout << endl << "Ответ z = " << z << endl;
}

int main()
{
	setlocale(LC_ALL, "RU");
	cout << "7|3 МиН-МаХ" << endl << endl;
	int armin[256], armax[256], k, n, min, max, z;
	EnterN(n);
	EnterAr(armin, k=1, n);
	EnterAr(armax, k=2, n);
	SearchMin(armin, min, n);
	SearchMax(armax, max, n);
	SearchZ(min, max, z);
	Answer(z);
}
//////////////////////////////////////////////////////////4
#include <iostream>
#include <locale>
using namespace std;

void EnterN(int &n)
{
	cout << "Введите N = ";
	cin >> n;
	cout << endl;
}

void EnterArr(int arr[][256], int n)
{
	cout << "Введите двумерный массив: " << endl;
	for (int i = 0; i < n; i++)
	{
		cout << "Элемент " << i + 1 << "-й " << "строки: ";
		for (int j = 0; j < n; j++)
			cin >> arr[i][j];
	}
}

void SearchS(int a[][256], int b[][256], int &s, int n)
{
	s = 0;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			s += a[i][j] + b[i][j];
}

void Answer(int s)
{
	cout << endl << "Ответ s = " << s << endl;
}

int main()
{
	setlocale(LC_ALL, "RU");
	cout << "7|4 Двумерный массив - Сумма" << endl << endl;
	int a[256][256], b[256][256], s, n;
	EnterN(n);
	EnterArr(a, n);
	EnterArr(b, n);
	SearchS(a, b, s, n);
	Answer(s);
}
//////////////////////////////////////////////////////////5
#include <iostream>
#include <locale>
using namespace std;

void EnterN(int &n)
{
	cout << "Введите N = ";
	cin >> n;
	cout << endl;
}

void EnterMatrix(int arr[][256], int n, int k)
{
	cout << "Введите "<< k <<"-ю матрицу " << n << "x" << n << ":" << endl;
	for (int i = 0; i<n; i++)
	{
		cout << "Элемент " << i + 1 << "-й " << "строки: ";
		for (int j = 0; j<n; j++)
			cin >> arr[i][j];
	}
	cout << endl << k <<"-ая матрица:" << endl;
	for (int i = 0; i<n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			cout.width(3);
			cout << arr[i][j];
		}
		cout << endl;
	}
	cout << endl;
}

void SearchMatrixC(int a[][256], int b[][256], int c[][256], int n)
{
	for (int i = 0; i<n; i++)
		for (int j = 0; j<n; j++)
			c[i][j] = 0;

	for (int i = 0; i<n; i++)
		for (int j = 0; j<n; j++)
			for (int m = 0; m<n; m++)
				c[i][j] += a[i][m] * b[m][j];
}

void AnswerC(int c[][256], int n)
{
	cout << "Итоговая матрица:" << endl;
	for (int i = 0; i<n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			cout.width(4);
			cout << c[i][j];
		}
		cout << endl;
	}
}

void main()
{
	setlocale(LC_ALL, "RU");
	cout << "7|5 Умножение матриц" << endl << endl;
	int a[256][256], b[256][256], c[256][256], n, k;
	EnterN(n);
	EnterMatrix(a, n, k = 1);
	EnterMatrix(b, n, k = 2);
	SearchMatrixC(a, b, c, n);
	AnswerC(c,n);
}