#define _USE_MATH_DEFINES 1

#include <iostream>
#include <locale.h>
#include <windows.h>
#include <math.h>

using namespace std;

/*
* Компилятор: MS VC++ 2008 eX
* Совместимость: Все допустимые компиляторы
* Задание 1
*/

double v(double r, double h)
{
	double V = M_PI * r * r * h;
	return V;
}

int main()
{
	double r, h;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите R и H" << endl;
	cin >> r >> h;
	cout << "Объем цилиндра = " << v(r, h) << endl;
}