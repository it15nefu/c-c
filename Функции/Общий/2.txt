#include <iostream>
#include <stdlib.h>
#include <math.h>

using namespace std;

void Calculate (float &x);

float x;

int main()
{
    cout << "x = ";
    cin >> x;
    Calculate (x);
    cout << x;

    return 0;
}

void Calculate (float &x)
{
    x = (sinh(x) * tan(2 * x + 1) - tan(1 + sinh(x + 1)) * tan(1 + sinh(x + 1)));
}