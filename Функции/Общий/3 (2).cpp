#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int* ArrayCreate (const int size);
int* ArrayRandomize (int* mtrx, const int size);
void ArrayPrint(int* arr, const int size);
int ArrayMax (int* arr, int size);
int ArrayMin (int* arr, int size);

int main()
{
    int n;

    printf("Size: ");
    scanf("%i", &n);

    int* mtrx = ArrayCreate(n);
    mtrx = ArrayRandomize(mtrx, n);
    ArrayPrint(mtrx, n);

    printf("%i", ArrayMin(mtrx, n) + ArrayMax(mtrx, n));

    return 0;
}

int* ArrayCreate (const int size)
{
    int* mtrx = new int[size];

    return mtrx;
}

int* ArrayRandomize (int* arr, const int size)
{
    srand(time(NULL));
    
    for(int i = 0; i < size; i++)
           arr[i] = rand() % 10;

    return arr;
}

int ArrayMax (int* arr, const int size)
{
    int max = 0;

    for (int i = 0; i < size; i++)
        if (arr[i] > arr[max])
            max = i;

    return arr[max];
}

int ArrayMin (int* arr, const int size)
{
    int min = 0;

    for (int i = 0; i < size; i++)
        if (arr[i] < arr[min])
            min = i;

    return arr[min];
}

void ArrayPrint(int* arr, const int size)
{
   for(int i = 0; i < size; i++)
        printf("%i ", arr[i]);
   printf("\n");
}