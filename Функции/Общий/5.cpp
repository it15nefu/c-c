#include <iostream>
#include <iomanip>
#include <locale>
#include <windows.h>

using namespace std;

/*
* Компилятор: MS VC++ 2008 eX
* Совместимость: Все допустимые компиляторы C++
* Задание 5
*/

void array_fill(int **a, int n, int m)
{
	for(int i=0; i<n; i++)
	{
		for(int j=0; j<m; j++)
		{
			a[i][j] = rand()%10;
			cout << a[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

void array_muliple(int **a, int **b, int **c, int n, int m)
{
	for(int i=0; i<n; i++)
		for(int j=0; j<m; j++)
			c[i][j] = 0;
	for(int i=0; i<n; i++)
		for(int j=0; j<m; j++)
			for(int k=0; k<n; k++)
				c[i][j] += a[i][k] * b[k][j];
}

void array_out(int **a, int n, int m)
{
	for(int i=0; i<n; i++)
	{
		for(int j=0; j<m; j++)
		{
			cout.width(6);
			cout.setf(ios::left);
			cout << a[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

int main()
{
	int n; //n строк
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите размер матрицы" << endl;
	cin >> n;
	int **a = new int*[n];
	int **b = new int*[n];
	int **c = new int*[n];
	for(int i=0; i<n; i++)
	{
		a[i] = new int[n];
		b[i] = new int[n];
		c[i] = new int[n];
	}
	array_fill(a, n, n);
	array_fill(b, n, n);
	array_muliple(a, b, c, n, n);
	array_out(c, n, n);
}