#include <iostream>

using namespace std;

float glasn(char a)
{
	switch(a)
	{
		case 'a': return 1;
		case 'e': return 1;
		case 'i': return 1;
		case 'o': return 1;
		case 'u': return 1;
		case 'y': return 1;
		default: return 0;
	}
	return 0;
}

void main()
{
	char a;
	cin >> a;
	cout << glasn(a) << endl;
}