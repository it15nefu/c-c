#include <iostream>
#include <math.h>

using namespace std;

double f(double a1, double a2, double b1, double b2, double c1, double c2)
{
	if(
		((b1-a1)*(b1-a1) + (b2-a2)*(b2-a2)) ==
		((c1-b1)*(c1-b1) + (c2-b2)*(c2-b2)) ==
		((a1-c1)*(a1-c1) + (a2-c2)*(a2-c2))
		)
		return 1;
	else
		return 0;
}

void main()
{
	double a1, b1, c1, a2, b2, c2;
	cin >> a1 >> a2 >> b1 >> b2 >> c1 >> c2;
	if(f(a1,a2,b1,b2,c1,c2))
		cout << "Da"<< endl;
	else
		cout << "Nyet" << endl;
}