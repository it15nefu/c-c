#include <iostream>
#include <math.h>
#include <time.h>

using namespace std;

void g(int **a, int n, int m)
{
	for(int i=0; i<n; i++)
		for(int j=0; j<m; j++)
			a[i][j] = rand() % 100;
}

int f(int **a, int n, int m)
{
	int f=0, k=0;
	for(int i=0; i<n; i++)
	{
		for(int j=0; j<m-1; j++)
			if(a[i][j]>a[i][j+1])
				f=1;
		if(!f)
			k++;
	}
	return k;
}

void main()
{
	int **a = new int*[10];
	for(int i=0; i<10; i++)
		a[i] = new int[10];
	g(a, 10, 10);
	cout << f(a, 10, 10) << endl;
}