#include <iostream>
#include <math.h>

using namespace std;

double g(double x, double y, double z)
{
	return (1+x+y+z)/(pow(exp(1.0),x)+cos(x+z));
}

void main()
{
	double a, b, c;
	cin >> a >> b >> c;
	cout << g(a,a-b,c+a)+g(c,3.8,a*a) << endl;
}