#include <iostream>
#include <locale>
#include <windows.h>

using namespace std;

int binom(int m, int n)
{
	if((n<m) || (n<0) || (m<0))
	{
		cout << "ОШИБКА!" << endl;
		return 0;
	}
	if(n==m || m==0)
		return 1;
	else
		return binom(m, n-1) + binom(m-1, n-1);
}

int main()
{
	int m, n;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите M и N" << endl;
	cin >> m >> n;
	cout << binom(m, n) << endl;
}
