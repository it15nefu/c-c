#include <iostream>
#include <locale>
#include <math.h>
#include <windows.h>

using namespace std;

void s_out(char *s)
{
	if(*s)
	{
		s_out(s+1);
		cout << *s;
	}
}

int main()
{
	char s[256];
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите строку" << endl;
	gets(s);
	s_out(s);
	cout << endl;
}
