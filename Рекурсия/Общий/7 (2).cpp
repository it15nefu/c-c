#include <iostream>

using namespace std;

int Euclid (int a, int b);

int main()
{
    int a, b;

    cout << "a, b = ";
    cin >> a >> b;
    cout << "HOD(" << a << ", " << b << ") = " << Euclid(a, b);

    return 0;
}

int Euclid (int a, int b)
{
    return b ? Euclid (b, a % b) : a;
}