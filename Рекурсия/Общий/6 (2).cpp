#include <iostream>

using namespace std;

void ReverseString (const char *s);

int main()
{
    char s[255];

    cout << "s = ";
    gets(s);
    ReverseString(s);
    return 0;
}

void ReverseString (const char *s){
    if (*s == '\0')
        return;
    ReverseString(s + 1);
    cout << *s;
}