#include <iostream>
#include <locale>
#include <windows.h>

using namespace std;

int A(int m, int n)
{
	if(m==0)
		return n + 1;
	if(n==0)
		return A(m-1, 1);
	return A(m-1, A(m, n-1));
}

int main()
{
	int m, n;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите N и M" << endl;
	cin >> n >> m;
	cout << A(m, n) << endl;
}
