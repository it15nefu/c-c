#include <iostream>

using namespace std;

int Akkerman (int m, int n);

int main()
{
    int m, n;

    cout << "m, n = ";
    cin >> m >> n;
    cout << "A(" << m << ", " << n << ") = " << Akkerman(m, n);

    return 0;
}

int Akkerman (int m, int n)
{
    if (m == 0)
        return n + 1;
    else if (m > 0 && n == 0)
        return Akkerman (m - 1, 1);
    else
        return Akkerman(m - 1, Akkerman(m, n - 1));
}