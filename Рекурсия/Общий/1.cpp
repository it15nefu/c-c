#include <iostream>
#include <locale>
#include <windows.h>

using namespace std;

int fact(int n)
{
	if(n<0)
	{
		cout << "ОШИБКА!" << endl;
		return 0;
	}
	return n < 1 ? 1 : n*fact(n-1);
}

int main()
{
	int n;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите N" << endl;
	cin >> n;
	cout << fact(n) << endl;
}
