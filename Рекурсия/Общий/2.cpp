#include <iostream>
#include <locale>
#include <windows.h>

using namespace std;

int poW(int n, int y)
{
	if(y<0)
	{
		cout << "ОШИБКА!" << endl;
		return 0;
	}
	return y < 1 ?  1 : n*poW(n, y-1);
}

int main()
{
	int n, y;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите N и Y" << endl;
	cin >> n >> y;
	cout << poW(n, y) << endl;
}
