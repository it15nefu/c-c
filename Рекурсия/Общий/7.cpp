#include <iostream>
#include <locale>
#include <windows.h>

using namespace std;

int gcd(int m, int n)
{
	return n ? gcd(n, m % n) : m;
}

int main()
{
	int m, n;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите M и N" << endl;
	cin >> m >> n;
	cout << gcd(m, n) << endl;
}
