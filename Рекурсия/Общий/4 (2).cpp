#include <iostream>

float Binom(float m, float n);

int main()
{
    float m, n;

    std :: cout << "m, n = ";
    std :: cin >> m >> n;
    std :: cout << "Binom(" << m << ", " << n << ") = " << Binom(m, n);

    return 0;
}

float Binom (float m, float n)
{
    return n ? (m * Binom (m - 1, n - 1)) / n : 1;
}