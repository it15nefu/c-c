#include <iostream>

using namespace std;

int Factorial (int n);

int main()
{
    int n;

    cout << "n = ";
    cin >> n;
    cout << n << "! = " << Factorial(n);

    return 0;
}

int Factorial (int n)
{
    return n > 0 ? n * Factorial(n - 1) : 1;
}
