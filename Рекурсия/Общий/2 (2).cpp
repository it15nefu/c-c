#include <iostream>

int Power (int a, int n);

int main()
{
    int a, n;

    std :: cout << "a, n = ";
    std :: cin >> a >> n;
    std :: cout << a << "^" << n << " = "<< Power(a, n);

    return 0;
}

int Power (int a, int n)
{
    return (!n) ? 1 : a * Power(a, n - 1);
}