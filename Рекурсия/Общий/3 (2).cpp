#include <iostream>

int Fibonacci (int n);

int main()
{
    int n;

    std :: cout << "n = ";
    std :: cin >> n;
    std :: cout << "F(" << n << ") = "<< Fibonacci(n);

    return 0;
}

int Fibonacci (int n)
{
    return (n <= 2) ? 1 : (Fibonacci(n - 1) + Fibonacci(n - 2));
}