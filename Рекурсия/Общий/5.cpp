#include <iostream>
#include <locale>
#include <math.h>
#include <windows.h>

using namespace std;

float sqrT(float m, float n)
{
	if(m<0 || n<0)
	{
		cout << "ОШИБКА" << endl;
		return 0;
	}
	return n <= 1 ? sqrt(m) : sqrt(m + sqrT(sqrt(m), n-1));
}

int main()
{
	float n;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите N" << endl;
	cin >> n;
	cout << sqrT(3.0, n) << endl;
}
