#include <iostream>
#include <locale>
#include <windows.h>

using namespace std;

int fibonacci(int n)
{
	if(n<0)
		return n > -3 ?  -1 : fibonacci(n+1)+fibonacci(n+2);
	if(n==0)
		return 0;
	return n < 3 ?  1 : fibonacci(n-1)+fibonacci(n-2);
}

int main()
{
	int n;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите N" << endl;
	cin >> n;
	cout << fibonacci(n) << endl;
}
