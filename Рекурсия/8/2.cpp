#define ИОСТРИМ	    <iostream>
#define ЛОКАЛЬ	    <locale>
#define ВИНДА	    <windows.h>
#define МАТЕМАТИКА  <math.h>
#define НАЧАЛО	    {
#define КОНЕЦ	    }
#define ГЛАВНЫЙ	    main
#define ЦЕЛЫЙ	    int
#define ПЛАВАЮЩИЙ   double
#define ВЕРНУТЬ	    return
#define КОНВВОД		cin
#define КОНВЫВОД	cout
#define	КНЦСТРОКИ	endl
#define	ЕЩЕ			<<
#define	еще			>>

#define	СТЕПЕНЬ(a,b)	pow(a,b)
#define КОДИРОВКА(a)	SetConsoleCP(a)
#define ВЫХКОДИРОВКА(a)	SetConsoleOutputCP(a)

#define ПРОСТРАНСТВО_ИМЕН	namespace
#define ИСПОЛЬЗОВАТЬ		using
#define СТАНДАРТ			std

#include ИОСТРИМ
#include ЛОКАЛЬ
#include ВИНДА
#include МАТЕМАТИКА

ИСПОЛЬЗОВАТЬ ПРОСТРАНСТВО_ИМЕН СТАНДАРТ;

/*
* Компилятор: QT, MS VC++
* Совместимость: Все допустимые компиляторы C++ (с поддержкой UTF-8)
* Задание В-8, 2
*/

ПЛАВАЮЩИЙ ИНТЕГРАЛ(ПЛАВАЮЩИЙ a, ПЛАВАЮЩИЙ b, ПЛАВАЮЩИЙ n, ПЛАВАЮЩИЙ x)
НАЧАЛО
    if(n==0)
		ВЕРНУТЬ СТЕПЕНЬ(exp(1.0), a*n)/a;
    if(n==1)
		ВЕРНУТЬ (-СТЕПЕНЬ(exp(1.0), a*x)*(a*sin(b*x)-b*cos(b*x)))/(a*a+b*b);
    if(n>1)
		ВЕРНУТЬ ((СТЕПЕНЬ(exp(1.0), a*x)*СТЕПЕНЬ(sin(b*x),n-1)*(a*sin(b*x)-b*cos(b*x)))/(a*a+n*n*b*b))+((n*(n-1)*b*b)/(a*a+n*n*b*b))*ИНТЕГРАЛ(a, b, n-2, x);
КОНЕЦ

ЦЕЛЫЙ ГЛАВНЫЙ()
НАЧАЛО
    ПЛАВАЮЩИЙ a, b, n, x;
    КОДИРОВКА(1251);
    ВЫХКОДИРОВКА(1251);
    КОНВЫВОД ЕЩЕ "Введите A, B, N и X" ЕЩЕ КНЦСТРОКИ;
    КОНВВОД еще a еще b еще n еще x;
    КОНВЫВОД ЕЩЕ ИНТЕГРАЛ(a, b, n, x) ЕЩЕ КНЦСТРОКИ;
КОНЕЦ
