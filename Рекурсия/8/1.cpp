#include <iostream>
#include <locale>
#include <windows.h>

using namespace std;

/*
* Компилятор: MS VC++ 2008 eX
* Совместимость: Все допустимые компиляторы C++
* Задание В-8, 1
*/

int DtoT(int n, int *a, int &k)
{
	if(n<1)
		return 1;
	a[k] = (n%3);
	k++;
	return DtoT(n/3, a, k);
}

int main()
{
	int n, a[30], k=0;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "Введите N" << endl;
	cin >> n;
	DtoT(n, a, k);
	for(int i=k-1; i>=0; i--)
		cout << a[i];
	cout << endl;
}
