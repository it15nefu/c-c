#include <iostream>
#include <locale>
#include <math.h>

using namespace std;

float integral(float x, float n)
{
 if(n>1)
  return (-(1/(n-1)))*(cos(x)/pow(sin(x), n-1))+((n-2)/(n-1))*integral(x, n-2);
 else if(n==1)
  return log(tan(x/2));
 else return x;
}

void main()
{
 float x, n;
 cin >> x >> n;
 cout << integral(x, n);
}