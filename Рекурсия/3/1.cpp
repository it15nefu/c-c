#include <iostream>
#include <locale>

using namespace std;

int apn(int s, int k, int n)
{
 if(n-1)
 {
  s = s + k;
  return apn(s, k, n-1);
 }
 return s;
}

int aps(int s, int k, int n)
{
 int v;
 if(n-1)
 {
  v = s + k;
  return s+aps(v, k, n-1);
 }
 return s;
}

void main()
{
 int n;
 cin >> n;
 cout << apn(3, 5, n) << endl;
 cout << aps(3, 5, n) << endl;
}